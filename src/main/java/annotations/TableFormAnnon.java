package annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author ravil
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface TableFormAnnon {

    String name() default "";

    String color() default "";

}
