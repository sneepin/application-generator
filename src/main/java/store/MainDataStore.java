package store;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ravil
 */
public class MainDataStore {

    private static final String FILES_PATH = "";

    private static MainDataStore instance;

    private Map<Class<?>, List> dataMap;

    public static MainDataStore getInstance() {
        if (instance == null) {
            instance = new MainDataStore();
        }
        return instance;
    }

    private MainDataStore() {
        dataMap = new LinkedHashMap<>();

    }

    public void putToMap(Class<?> modelClass, List<?> list) {
        dataMap.put(modelClass, list);
    }

    public <T> List<T> getList(Class<T> modelClass) {
        return dataMap.get(modelClass);
    }
}
