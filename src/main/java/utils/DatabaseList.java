package utils;

import app.ConnectionCreater;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DatabaseList<T> extends ArrayList<T> {

    private Type type;
    private Dao dao;

    public DatabaseList(Class type) {
        super();

        try {
            dao = DaoManager.createDao(ConnectionCreater.createConnectionSource(), type);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.type = type;

        try {
            List l = dao.queryForAll();

            addAll(l);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Type getType() {
        return type;
    };

    @Override
    public boolean add(T t) {
        new Thread(() -> {
            try {
                dao.createIfNotExists(t);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();

        return super.add(t);
    }

    @Override
    public T remove(int index) {
        new Thread(() -> {
            try {
                dao.delete(get(index));
            } catch (SQLException e) {
                // ignore
            } catch (IndexOutOfBoundsException e) {
                // ignore
            }
        }).start();

        return super.remove(index);
    }

    @Override
    public boolean remove(Object o) {
        new Thread(() -> {
            try {
                dao.delete(o);
            } catch (SQLException e) {
                // ignore
            } catch (IndexOutOfBoundsException e) {
                // ignore
            }
        }).start();

        return super.remove(o);
    }
}
