package view;

import javax.swing.table.TableModel;
import java.util.List;

/**
 * @author ravil
 */
public interface TableView<T> {

    void showEditPanel(T objToEdit);

    void showTable();

    void showFoxProView(T curObject);

    boolean resultErrorDialog();

    boolean resultConfirmDialog();

    void update();

    void showData(List<T> data);

    void setSelectedPosition(int position);

    void exit();
}
