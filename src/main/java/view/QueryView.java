package view;

import java.util.List;

/**
 * @author ravil
 */
public interface QueryView<T> {

    void setSelectedPosition(int position);

    void showData(List<T> data);

}
