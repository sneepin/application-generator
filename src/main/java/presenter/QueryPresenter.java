package presenter;

import view.QueryView;

import java.util.List;

/**
 * @author ravil
 */
public class QueryPresenter<T> {

    private static final int TABLE_STATE = 0;

    private QueryView<T> tQueryView;
    private List<T> data;

    private T currentObject;
    private int curObjectPosition;

    public QueryPresenter(QueryView<T> tableView, List<T> data) {
        this.tQueryView = tableView;
        this.data = data;

        curObjectPosition = 0;
    }

    public void toBeginButtonClicked() {
        if (!data.isEmpty()) {
            tQueryView.setSelectedPosition(0);
            curObjectPosition = 0;
            currentObject = data.get(0);
        }
    }

    public void prevButtonClicked() {
        int nextPos = curObjectPosition - 1;

        if (nextPos < 0) {
            int last = data.size() - 1;
            tQueryView.setSelectedPosition(last);
            curObjectPosition = last;
            currentObject = data.get(last);
        }

        if (nextPos >= 0) {
            tQueryView.setSelectedPosition(nextPos);
            curObjectPosition = nextPos;
            currentObject = data.get(nextPos);
        }
    }

    public void nextButtonClicked() {
        int nextPos = curObjectPosition + 1;

        int lastPos = data.size() - 1;

        if (nextPos > lastPos) {
            tQueryView.setSelectedPosition(0);
            curObjectPosition = 0;
            currentObject = data.get(0);
        }

        if (nextPos <= lastPos) {
            tQueryView.setSelectedPosition(nextPos);
            curObjectPosition = nextPos;
            currentObject = data.get(nextPos);
        }
    }

    public void toLastButtonClicked() {
        if (!data.isEmpty()) {
            tQueryView.setSelectedPosition(data.size() - 1);
            curObjectPosition = data.size() - 1;
            currentObject = data.get(curObjectPosition);
        }
    }
}
