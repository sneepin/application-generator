package presenter;

import app.ConnectionCreater;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import store.MainDataStore;
import view.TableView;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

/**
 * @author ravil
 */
public class TablePresenter<T> {

    private static final int TABLE_STATE = 0;
    private static final int FOX_PRO_STATE = 1;

    private TableView<T> tableView;
    private List<T> data;
    private int currentState;


    private T currentObject;
    private int curObjectPosition;

    public TablePresenter(TableView<T> tableView) {
        this.tableView = tableView;
    }

    public void init(Class<T> modelClass) {
        this.data = MainDataStore.getInstance().getList(modelClass);

        currentState = TABLE_STATE;
        tableView.showData(data);
    }

    public void addButtonClicked() {
        tableView.showEditPanel(null);
    }

    public void editButtonClicked(int position) {
        if (currentState == TABLE_STATE) {
            if (tableView.resultErrorDialog()) {
                return;
            }

            tableView.showEditPanel(data.get(position));
        } else {
            tableView.showEditPanel(data.get(curObjectPosition));
        }
    }

    public void deleteButtonClicked(int position) {
        if (currentState == TABLE_STATE) {
            if (tableView.resultErrorDialog()) {
                return;
            }
        }


        int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to delete this record?");

        System.out.println(dialogResult);

        if(dialogResult == JOptionPane.YES_OPTION) {
            if (currentState == TABLE_STATE) {
                data.remove(data.get(position));
                tableView.update();
            } else {
                data.remove(curObjectPosition);

                if (curObjectPosition >= data.size()) {
                    tableView.showFoxProView(data.get(0));
                    curObjectPosition = 0;
                    currentObject = data.get(0);
                } else {
                    tableView.showFoxProView(data.get(curObjectPosition));
                }
            }
        };
    }

    public void toBeginButtonClicked() {
        if (currentState == TABLE_STATE) {
            tableView.setSelectedPosition(0);
            return;
        }

        if (!data.isEmpty()) {
            tableView.showFoxProView(data.get(0));
            curObjectPosition = 0;
            currentObject = data.get(0);
        }
    }

    public void prevButtonClicked() {
        int nextPos = curObjectPosition - 1;

        if (nextPos < 0) {
            int last = data.size() - 1;

            curObjectPosition = last;
            currentObject = data.get(last);
            if (currentState == TABLE_STATE) {
                tableView.setSelectedPosition(last);
                return;
            } else {
                tableView.showFoxProView(data.get(last));
            }
        }

        if (nextPos >= 0) {
            curObjectPosition = nextPos;
            currentObject = data.get(nextPos);
            if (currentState == TABLE_STATE) {
                tableView.setSelectedPosition(nextPos);
            } else {
                tableView.showFoxProView(data.get(nextPos));
            }
        }
    }

    public void nextButtonClicked() {
        int nextPos = curObjectPosition + 1;

        int lastPos = data.size() - 1;

        if (nextPos > lastPos) {
            curObjectPosition = 0;
            currentObject = data.get(0);
            if (currentState == TABLE_STATE) {
                tableView.setSelectedPosition(0);
            } else {
                tableView.showFoxProView(data.get(0));
            }
        }

        if (nextPos <= lastPos) {
            curObjectPosition = nextPos;
            currentObject = data.get(nextPos);
            if (currentState == TABLE_STATE) {
                tableView.setSelectedPosition(nextPos);
            } else {
                tableView.showFoxProView(data.get(nextPos));
            }
        }
    }

    public void toLastButtonClicked() {
        if (!data.isEmpty()) {
            curObjectPosition = data.size() - 1;
            currentObject = data.get(curObjectPosition);
            if (currentState == TABLE_STATE) {
                tableView.setSelectedPosition(data.size() - 1);
            } else {
                tableView.showFoxProView(data.get(data.size() - 1));
            }
        }
    }

    public void changeViewTypeClicked(int position) {
        if (currentState == TABLE_STATE) {
            currentObject = data.get(position == -1 ? 0 : position);
            curObjectPosition = position == -1 ? 0 : position;
            currentState = FOX_PRO_STATE;
            tableView.showFoxProView(currentObject);
            return;
        }

        if (currentState == FOX_PRO_STATE) {
            tableView.showTable();
            currentState = TABLE_STATE;
        }
    }

    public void onExitClicked() {
        tableView.exit();
    }

    public void onOkButtonClicked(T obj, boolean isAdd) {
        if (isAdd) {
            data.add(obj);
        }

        if (currentState == FOX_PRO_STATE) {
            if (isAdd) {
                tableView.showFoxProView(data.get(data.size() - 1));
            } else {
                tableView.showFoxProView(data.get(curObjectPosition));
            }
        }

        if (currentState == TABLE_STATE) {
            tableView.showTable();
            tableView.update();
        }

        new Thread(() -> {
            try {
                Dao dao = DaoManager.createDao(ConnectionCreater.createConnectionSource(), obj.getClass());
                dao.createOrUpdate(obj);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void onCancelButtonClicked() {
        int dialogResult = JOptionPane.showConfirmDialog(null, "Вы хотите отменить?");
        if (dialogResult == JOptionPane.YES_OPTION) {
            if (currentState == TABLE_STATE) {
                tableView.showTable();
            }
            if (currentState == FOX_PRO_STATE) {
                tableView.showFoxProView(currentObject);
            }
        }
    }

    private void doAdd(T obj) {

    }

    private void doDelete(T obj) {

    }

}
