import app.ConnectionCreater;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import forms.MyMenu;
import forms.QueryForm;
import model.AssemblyOrFinishedPart;
import model.Profession;
import model.Sign;
import model.SpentTimes;
import table_model.BaseTableModel;
import forms.EditPanel;
import store.MainDataStore;
import forms.TableForm;
import utils.DatabaseList;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author ravil
 */
public class Main {

    public static void main(String[] args) {
        ConnectionSource connectionSource = null;
        try {
             connectionSource = ConnectionCreater.createConnectionSource();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            TableUtils.createTableIfNotExists(connectionSource, Sign.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            TableUtils.createTableIfNotExists(connectionSource, Profession.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            TableUtils.createTableIfNotExists(connectionSource, AssemblyOrFinishedPart.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            TableUtils.createTableIfNotExists(connectionSource, SpentTimes.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        DatabaseList<Profession> professions = new DatabaseList<>(Profession.class);
        MainDataStore.getInstance().putToMap(Profession.class, professions);

        List<AssemblyOrFinishedPart> parts = new DatabaseList<>(AssemblyOrFinishedPart.class);
        MainDataStore.getInstance().putToMap(AssemblyOrFinishedPart.class, parts);

        DatabaseList<Sign> signs = new DatabaseList<>(Sign.class);
//        signs.add(new Sign("сборочная единица"));
//        signs.add(new Sign("готовая деталь"));
        MainDataStore.getInstance().putToMap(Sign.class, signs);

        DatabaseList<SpentTimes> spentTimes = new DatabaseList<>(SpentTimes.class);
        MainDataStore.getInstance().putToMap(SpentTimes.class, spentTimes);


        TableForm professionTable = new TableForm(new BaseTableModel(professions, Profession.class),
                new EditPanel(Profession.class), Profession.class);

        TableForm partsTable = new TableForm(new BaseTableModel(parts, AssemblyOrFinishedPart.class),
                new EditPanel(AssemblyOrFinishedPart.class), AssemblyOrFinishedPart.class);

        TableForm spentTimesTable = new TableForm(new BaseTableModel(spentTimes, SpentTimes.class),
                new EditPanel(SpentTimes.class), SpentTimes.class);


        String[] queries = new String[] {"Первый запрос: Професси выполнившие план",
                "Второй запрос: Готовые детали ",
                "Третий запрос: Затраченое время больше часа"};

        Consumer<String> queryConsumer = (query) -> {
            if (query.equals(queries[0])) {
                List<Profession> data = executeQuery1();

                new QueryForm<Profession>(data, new BaseTableModel(data, Profession.class)).setVisible(true);
            } else if (query.equals(queries[1])) {
                List<SpentTimes> data = executeQuery2();

                new QueryForm<SpentTimes>(data, new BaseTableModel(data, SpentTimes.class)).setVisible(true);
            } else if (query.equals(queries[2])) {
                List<AssemblyOrFinishedPart> data = executeQuery3();

                new QueryForm<AssemblyOrFinishedPart>(data, new BaseTableModel(data, AssemblyOrFinishedPart.class)).setVisible(true);
            }
        };

        MyMenu myMenu = new MyMenu(professionTable, partsTable, spentTimesTable, queries, queryConsumer);
        myMenu.setVisible(true);
    }

    private static List<Profession> executeQuery1() {
        try {
            Dao dao = DaoManager.createDao(ConnectionCreater.createConnectionSource(), Profession.class);

            QueryBuilder queryBuilder = dao.queryBuilder();

//            queryBuilder.orderB("nameProfession");
//
            return dao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private static List<SpentTimes> executeQuery2() {
        try {
            Dao dao = DaoManager.createDao(ConnectionCreater.createConnectionSource(), SpentTimes.class);

            QueryBuilder queryBuilder = dao.queryBuilder();

//            queryBuilder.orderB("nameProfession");
//
            return dao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private static List<AssemblyOrFinishedPart> executeQuery3() {
        try {
            Dao dao = DaoManager.createDao(ConnectionCreater.createConnectionSource(), AssemblyOrFinishedPart.class);

            QueryBuilder queryBuilder = dao.queryBuilder();

//            queryBuilder.orderB("nameProfession");
//
            return dao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
