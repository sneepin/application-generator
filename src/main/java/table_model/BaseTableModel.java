package table_model;

import annotations.FieldForm;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BaseTableModel<T> extends AbstractTableModel {

    private List<T> data;

    private List<String> columns;

    private List<Field> fields;

    public BaseTableModel(List<T> data, Class<T> sourceClass) {
        this.data = data;
        this.columns = new ArrayList<>();
        this.fields = new ArrayList<>();

        buildColumns(sourceClass);
    }

    private void buildColumns(Class<T> sourceClass) {
        Field[] fields = sourceClass.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);

            FieldForm fieldForm = field.getAnnotation(FieldForm.class);
            if (fieldForm != null) {
                columns.add(fieldForm.ruName());
                this.fields.add(field);
            }
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public String getColumnName(int column) {
        return columns.get(column);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        T obj = data.get(rowIndex);

        Object value = null;
        try {
            value = this.fields.get(columnIndex).get(obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (value instanceof Boolean) {
            return (Boolean) value ? "Да" : "Нет";
        }

        return value != null ? value : "";
    }

    public void setData(List<T> data) {
        this.data = data;
        fireTableDataChanged();
    }
}
