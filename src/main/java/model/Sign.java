package model;

import annotations.TableFormAnnon;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "signs")
@TableFormAnnon
public class Sign {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String nameSign;

    public Sign() {
    }

    public Sign(String nameSign) {
        this.nameSign = nameSign;
    }

    public String getNameSign() {
        return nameSign;
    }

    public void setNameSign(String nameSign) {
        this.nameSign = nameSign;
    }

    @Override
    public String toString() {
        return nameSign;
    }
}
