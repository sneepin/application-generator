package model;

import annotations.FieldForm;
import annotations.TableFormAnnon;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "professions")
@TableFormAnnon(name = "Професси")
public class Profession {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    @FieldForm(ruName = "Наименование професии")
    private String nameProfession;

    @DatabaseField
    @FieldForm(ruName = "Количество сделанных деталей")
    private int detailsCount;

    public Profession() {
    }

    public Profession(String nameProfession) {
        this.nameProfession = nameProfession;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameProfession() {
        return nameProfession;
    }

    public void setNameProfession(String nameProfession) {
        this.nameProfession = nameProfession;
    }

    public int getDetailsCount() {
        return detailsCount;
    }

    public void setDetailsCount(int detailsCount) {
        this.detailsCount = detailsCount;
    }

    @Override
    public String toString() {
        return nameProfession;
    }
}
