package model;

import annotations.FieldForm;
import annotations.TableFormAnnon;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "times")
@TableFormAnnon(name = "Затраченное время")
public class SpentTimes {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField(canBeNull = true, foreign = true,  foreignAutoCreate = true, foreignAutoRefresh = true)
    @FieldForm(ruName = "Наименование детали")
    private AssemblyOrFinishedPart part;

    @DatabaseField(canBeNull = true, foreign = true,  foreignAutoCreate = true, foreignAutoRefresh = true)
    @FieldForm(ruName = "Наименование профессии")
    private Profession profession;

    @DatabaseField
    @FieldForm(ruName = "Затрачено времени на подготовку")
    private int spentBeforeTime;

    @DatabaseField
    @FieldForm(ruName = "Затрачено времени на изготовление")
    private int spentForOperationTime;

    public SpentTimes() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AssemblyOrFinishedPart getPart() {
        return part;
    }

    public void setPart(AssemblyOrFinishedPart part) {
        this.part = part;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public int getSpentBeforeTime() {
        return spentBeforeTime;
    }

    public void setSpentBeforeTime(int spentBeforeTime) {
        this.spentBeforeTime = spentBeforeTime;
    }

    public int getSpentForOperationTime() {
        return spentForOperationTime;
    }

    public void setSpentForOperationTime(int spentForOperationTime) {
        this.spentForOperationTime = spentForOperationTime;
    }
}
