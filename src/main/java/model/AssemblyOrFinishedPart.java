package model;


import annotations.FieldForm;
import annotations.TableFormAnnon;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "parts")
@TableFormAnnon(name = "Детали")
public class AssemblyOrFinishedPart {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = true, foreign = true,  foreignAutoCreate = true, foreignAutoRefresh = true)
    @FieldForm(ruName = "Тип")
    private Sign sign;

    @DatabaseField
    @FieldForm(ruName = "Номер госта")
    private String gostNumber;

    @DatabaseField
    @FieldForm(ruName = "Дата госта")
    private String gostDate;

    @DatabaseField
    @FieldForm(ruName = "Наименование")
    private String nomination;

    public AssemblyOrFinishedPart() {
    }

    public AssemblyOrFinishedPart(String nomination) {
        this.nomination = nomination;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sign getSign() {
        return sign;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
    }

    public String getGostNumber() {
        return gostNumber;
    }

    public void setGostNumber(String gostNumber) {
        this.gostNumber = gostNumber;
    }

    public String getGostDate() {
        return gostDate;
    }

    public void setGostDate(String gostDate) {
        this.gostDate = gostDate;
    }

    public String getNomination() {
        return nomination;
    }

    public void setNomination(String nomination) {
        this.nomination = nomination;
    }

    @Override
    public String toString() {
        return nomination;
    }
}
