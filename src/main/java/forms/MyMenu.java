package forms;

import java.util.function.Consumer;

/**
 * @author ravil
 */
public class MyMenu extends BaseMenuForm {

    private TableForm tableForm1;

    private TableForm tableForm2;

    private TableForm tableForm3;

    private String[] queries;

    private Consumer<String> consumer;

    public MyMenu(TableForm tableForm1, TableForm tableForm2, TableForm tableForm3, String[] queries, Consumer<String> queryConsumer) {
        this.tableForm1 = tableForm1;
        this.tableForm2 = tableForm2;
        this.tableForm3 = tableForm3;

        this.queries = queries;
        this.consumer = queryConsumer;

        for (String s : queries) {
            queryCombo.addItem(s);
        }
    }

    @Override
    protected void setView() {
        tableButton1.setText("Профессии");
        tableButton2.setText("Детали");
        tableButton3.setText("Затраченное");

    }

    @Override
    protected void onTableButton1Clicked() {
        tableForm1.setVisible(true);
    }

    @Override
    protected void onTableButton2Clicked() {
        tableForm2.setVisible(true);
    }

    @Override
    protected void onTableButton3Clicked() {
        tableForm3.setVisible(true);
    }


    @Override
    protected void onQueryButtonClick() {
        consumer.accept((String) queryCombo.getSelectedItem());
    }
}
