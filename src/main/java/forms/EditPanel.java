package forms;

import annotations.FieldForm;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import store.MainDataStore;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;


public  class EditPanel<T> extends JPanel {

    public JPanel generatePanel = new JPanel();
    public JPanel buttonsPanel = new JPanel();
    public JButton okButton = new JButton();
    public JButton cancelButton = new JButton();

    protected T currenObject;
    protected TableForm tableForm;

    private Class<T> objectClass;
    private List<Consumer<T>> functions;

    public EditPanel(Class<T> objectClass){
        super();
        this.objectClass = objectClass;
        functions = new LinkedList<>();

        initComponents();

        try {
            generateView(objectClass.getDeclaredFields(), null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void generateView(Field[] fields, T objectToEdit) throws IllegalAccessException {
        functions.clear();
        generatePanel.removeAll();
        generatePanel.revalidate();
        generatePanel.repaint();

        for (Field field : fields) {
            field.setAccessible(true);

            // Add text label
            FieldForm fieldForm = field.getDeclaredAnnotation(FieldForm.class);
            if (fieldForm == null) {
                continue;
            }

            String ruName = fieldForm.ruName();
            JLabel label = new JLabel(ruName);
            generatePanel.add(label);

            // add component
            Class type = field.getType();
            if (type == boolean.class) {
                JCheckBox checkBox = new JCheckBox();

                if (objectToEdit != null) {
                    boolean value = (boolean) field.get(objectToEdit);

                    checkBox.setSelected(value);
                }

                Consumer<T> consumer = (T obj) -> {
                    try {
                        field.set(obj, checkBox.isSelected());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                };

                functions.add(consumer);

                generatePanel.add(checkBox);

            } else if (type.isPrimitive() || type == String.class) {
                JTextField textField = new JTextField("");
                textField.setToolTipText(field.getName());
                textField.setPreferredSize(new Dimension(150, 50));
                textField.setMaximumSize(new Dimension(100, 100));
                textField.setSize(100, 100);

                if (objectToEdit != null) {
                    Object value = field.get(objectToEdit);

                    if (value != null) {
                        textField.setText(value.toString());
                        textField.setMaximumSize(new Dimension(100, 50));
                    }
                }


                Consumer<T> consumer = (T obj) -> {
                    try {
                        if (type == String.class) {
                            field.set(obj, textField.getText());
                        } else if (type == int.class) {
                            String a = textField.getText();
                            String b = textField.getToolTipText();

                            try {
                                field.set(obj, Integer.parseInt(textField.getText()));
                            } catch (Exception e) {
                                field.set(obj, 0);
                            }

                        } else if (type == double.class) {

                            try {
                                field.set(obj, Double.parseDouble(textField.getText()));
                            } catch (Exception e) {
                                field.set(obj, 0d);
                            }


                        }

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                };

                functions.add(consumer);


                generatePanel.add(textField);
            } else  {
                List<?> list = MainDataStore.getInstance().getList(type);
                JComboBox box = new JComboBox(list.toArray());

                if (objectToEdit != null) {
                    Object value = field.get(objectToEdit);
                    box.setSelectedItem(value);
                }

                Consumer<T> consumer = (T obj) -> {
                    try {
                        field.set(obj, box.getSelectedItem());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                };

                functions.add(consumer);

                generatePanel.add(box);
            }
        }
    }

    public T getObjectToEdit() {
        return currenObject;
    }

    public void setObjectToEdit(T objectToEdit) {
        this.currenObject = objectToEdit;

        try {
            generateView(objectClass.getDeclaredFields(), objectToEdit);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void onSave() throws IllegalAccessException, InstantiationException {
        boolean isAdd = false;
        if (currenObject == null) {
            isAdd = true;
            currenObject = objectClass.newInstance();
        }

        for (Consumer<T> consumer : functions) {
            consumer.accept(currenObject);
        }

        System.out.print("");

        tableForm.onOkClicked(currenObject, isAdd);
    }

    public TableForm getTableForm() {
        return tableForm;
    }

    public void setTableForm(TableForm tableForm) {
        this.tableForm = tableForm;
    }

    private void initComponents() {
        this.setBackground(new Color(217, 217, 217, 232));
        this.setLayout(new FormLayout(
                "default, $lcgap, 80dlu:grow, $lcgap, default",
                "default, $lgap, 60dlu:grow, $lgap, 40dlu, $lgap, default"));

        //======== generatePanel ========
        {
            generatePanel.setLayout(new GridLayout(10,2));
//            generatePanel.setLayout(new BoxLayout(generatePanel, BoxLayout.X_AXIS));
//            generatePanel.setLayout(new FormLayout(
//                    "10dlu, 100dlu, 10dlu, 10dlu:grow, 5dlu",
//                    "fill:20dlu, 10dlu, fill:20dlu"));
            generatePanel.setSize(new Dimension(600, 700));

        }
        this.add(generatePanel, CC.xy(3, 3));

        //======== buttonsPanel ========
        {
            buttonsPanel.setBackground(new Color(217, 217, 217, 232));
            buttonsPanel.setLayout(new FormLayout(
                    "150dlu, $lcgap, 50dlu, $lcgap, 35dlu, $lcgap, 50dlu",
                    "default"));

            //---- okButton ----
            okButton.setText("\u041e\u041a");
            okButton.addActionListener(e -> {
                try {
                    onSave();
                } catch (IllegalAccessException | InstantiationException e1) {
                    e1.printStackTrace();
                }
            });
            buttonsPanel.add(okButton, CC.xy(3, 1));

            //---- cancelButton ----
            cancelButton.setText("\u041e\u0442\u043c\u0435\u043d\u0430");
            cancelButton.addActionListener(e -> tableForm.onCancelButtonClicked());
            buttonsPanel.add(cancelButton, CC.xy(7, 1));
        }
        this.add(buttonsPanel, CC.xy(3, 5));
    }

    public void showSaveButton(boolean isShown) {
        okButton.setVisible(isShown);
    }

    public void showCancelButton(boolean isShown) {
        cancelButton.setVisible(isShown);
    }

    public void setEnabledElements(boolean isEnabled) {
        Component[] components = generatePanel.getComponents();

        for (Component component : components) {
            if (component instanceof JTextField) {
                ((JTextField) component).setEditable(isEnabled);
            }
        }
    }

    public interface SaverInterface {

        void save();
    }
}
