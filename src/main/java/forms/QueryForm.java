package forms;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import presenter.QueryPresenter;
import table_model.BaseTableModel;
import view.QueryView;
/*
 * Created by JFormDesigner on Wed May 11 23:57:38 MSK 2016
 */



/**
 * @author Huan
 */
public class QueryForm<T> extends JFrame implements QueryView<T> {

    private QueryPresenter<T> presenter;

    public QueryForm(List<T> data, BaseTableModel tableModel) {
        initComponents();

        presenter = new QueryPresenter<>(this, data);

        table2.setModel(tableModel);

        if (tableModel.getRowCount() != 0) {
            table2.setRowSelectionInterval(0, 0);
        }

//        table2.setRowSelectionInterval(0, 0);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        jTitle2 = new JLabel();
        scrollPane2 = new JScrollPane();
        table2 = new JTable();
        panel2 = new JPanel();
        beginButton2 = new JButton();
        prevButton2 = new JButton();
        nextButton2 = new JButton();
        endButton2 = new JButton();
        viewButton2 = new JButton();
        exitButton2 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
            "10dlu, 80dlu:grow, 10dlu",
            "25dlu, $lgap, 170dlu:grow, $lgap, 10dlu:grow"));

        //---- jTitle2 ----
        jTitle2.setText("\u0420\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442");
        jTitle2.setFont(jTitle2.getFont().deriveFont(jTitle2.getFont().getStyle() & ~Font.BOLD, 18f));
        contentPane.add(jTitle2, CC.xy(2, 1, CC.CENTER, CC.DEFAULT));

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(table2);
        }
        contentPane.add(scrollPane2, CC.xy(2, 3));

        //======== panel2 ========
        {
            panel2.setLayout(new FormLayout(
                "3*(5dlu:grow, $lcgap), 30dlu, $lcgap, 29dlu, 2*($lcgap, 25dlu), $lcgap, 28dlu, 2*($lcgap, 4dlu:grow)",
                "default"));

            //---- beginButton2 ----
            beginButton2.setText("<<-");
            beginButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    presenter.toBeginButtonClicked();
                }
            });
            panel2.add(beginButton2, CC.xy(9, 1));

            //---- prevButton2 ----
            prevButton2.setText("<-");
            prevButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    presenter.prevButtonClicked();
                }
            });
            panel2.add(prevButton2, CC.xy(11, 1));

            //---- nextButton2 ----
            nextButton2.setText("->");
            nextButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    presenter.nextButtonClicked();
                }
            });
            panel2.add(nextButton2, CC.xy(13, 1));

            //---- endButton2 ----
            endButton2.setText("->>");
            endButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    presenter.toLastButtonClicked();
                }
            });
            panel2.add(endButton2, CC.xy(15, 1));

            //---- viewButton2 ----
            viewButton2.setText("\u0421\u043c\u0435\u043d\u0438\u0442\u044c \u0432\u0438\u0434");
            viewButton2.setVisible(false);
            panel2.add(viewButton2, CC.xy(17, 1));

            //---- exitButton2 ----
            exitButton2.setText("\u0412\u044b\u0445\u043e\u0434");
            exitButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    dispose();
                }
            });
            panel2.add(exitButton2, CC.xy(19, 1));
        }
        contentPane.add(panel2, CC.xy(2, 5));
        setSize(870, 450);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel jTitle2;
    private JScrollPane scrollPane2;
    private JTable table2;
    private JPanel panel2;
    private JButton beginButton2;
    private JButton prevButton2;
    private JButton nextButton2;
    private JButton endButton2;
    private JButton viewButton2;
    private JButton exitButton2;


    @Override
    public void setSelectedPosition(int position) {
        table2.setRowSelectionInterval(position, position);
    }

    @Override
    public void showData(List<T> data) {

    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
