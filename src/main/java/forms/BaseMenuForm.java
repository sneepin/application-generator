package forms;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Sat May 07 19:24:35 MSK 2016
 */



/**
 * @author Huan
 */
public abstract class BaseMenuForm extends JFrame {
    public BaseMenuForm() {
        initComponents();

        setView();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        tableButton1 = new JButton();
        tableButton2 = new JButton();
        tableButton3 = new JButton();
        queryCombo = new JComboBox();
        queryButton = new JButton();

        //======== this ========
        setTitle("\u041c\u0435\u043d\u044e");
        Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
            "10dlu, default:grow, 10dlu",
            "3*(fill:5dlu:grow, fill:8dlu:grow), 2*(5dlu:grow, fill:8dlu:grow), 5dlu:grow"));

        //---- tableButton1 ----
        tableButton1.setText("text");
        tableButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onTableButton1Clicked();
            }
        });
        contentPane.add(tableButton1, CC.xy(2, 2));

        //---- tableButton2 ----
        tableButton2.setText("text");
        tableButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onTableButton2Clicked();
            }
        });
        contentPane.add(tableButton2, CC.xy(2, 4));

        //---- tableButton3 ----
        tableButton3.setText("text");
        tableButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onTableButton3Clicked();
            }
        });
        contentPane.add(tableButton3, CC.xy(2, 6));
        contentPane.add(queryCombo, CC.xy(2, 8));

        //---- queryButton ----
        queryButton.setText("\u0417\u0430\u043f\u0440\u043e\u0441\u044b");
        queryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onQueryButtonClick();
            }
        });
        contentPane.add(queryButton, CC.xy(2, 10));
        setSize(400, 370);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    protected abstract void setView();

    protected abstract void onTableButton1Clicked();

    protected abstract void onTableButton2Clicked();

    protected abstract void onTableButton3Clicked();

    protected abstract void onQueryButtonClick();

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    protected JButton tableButton1;
    protected JButton tableButton2;
    protected JButton tableButton3;
    protected JComboBox queryCombo;
    protected JButton queryButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
