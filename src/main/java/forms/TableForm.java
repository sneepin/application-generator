package forms;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;

import annotations.TableFormAnnon;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import table_model.BaseTableModel;
import presenter.TablePresenter;
import view.TableView;
/*
 * Created by JFormDesigner on Sat May 07 20:09:29 MSK 2016
 */



/**
 * @author Huan
 */
public class TableForm<T> extends JFrame implements TableView<T> {

    private TablePresenter tablePresenter;

    private BaseTableModel<T> tableModel;
    private EditPanel editPanel;

    public TableForm(BaseTableModel<T> tableModel, EditPanel editPanel, Class<T> modelClass) {
        initComponents();

//        beginButton.setVisible(false);
//        prevButton.setVisible(false);
//        nextButton.setVisible(false);
//        endButton.setVisible(false);

        this.tableModel = tableModel;
        table1.setModel(this.tableModel);
        this.editPanel = editPanel;
        this.editPanel.setTableForm(this);

        tablePresenter = new TablePresenter<T>(this);
        tablePresenter.init(modelClass);

        if (tableModel.getRowCount() != 0) {
            table1.setRowSelectionInterval(0, 0);
        }

        TableFormAnnon annon = modelClass.getAnnotation(TableFormAnnon.class);
        if (annon != null) {
            jTitle.setText(annon.name());
        }
    }

    private void addButtonMouseClicked(MouseEvent e) {
        scrollPane1.setViewportView(panel2);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        jTitle = new JLabel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        panel1 = new JPanel();
        addButton = new JButton();
        editButton = new JButton();
        deleteButton = new JButton();
        beginButton = new JButton();
        prevButton = new JButton();
        nextButton = new JButton();
        endButton = new JButton();
        viewButton = new JButton();
        exitButton = new JButton();
        panel2 = new JPanel();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
            "10dlu, default:grow, 10dlu",
            "20dlu, fill:170dlu:grow, 10dlu, fill:20dlu, 10dlu"));

        //---- jTitle ----
        jTitle.setText("\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a");
        jTitle.setFont(jTitle.getFont().deriveFont(jTitle.getFont().getStyle() & ~Font.BOLD, 18f));
        contentPane.add(jTitle, CC.xy(2, 1, CC.CENTER, CC.DEFAULT));

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1, CC.xy(2, 2));

        //======== panel1 ========
        {
            panel1.setLayout(new FormLayout(
                "3*(5dlu:grow, $lcgap), 30dlu, $lcgap, 29dlu, 2*($lcgap, 25dlu), $lcgap, 28dlu, 2*($lcgap, 4dlu:grow)",
                "default"));

            //---- addButton ----
            addButton.setText("\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c");
            addButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    addButtonMouseClicked(e);
                }
            });
            addButton.addActionListener(e -> tablePresenter.addButtonClicked());
            panel1.add(addButton, CC.xy(1, 1));

            //---- editButton ----
            editButton.setText("\u0418\u0437\u043c\u0435\u043d\u0438\u0442\u044c");
            editButton.addActionListener(e -> tablePresenter.editButtonClicked(table1.getSelectedRow()));
            panel1.add(editButton, CC.xy(3, 1));

            //---- deleteButton ----
            deleteButton.setText("\u0423\u0434\u0430\u043b\u0438\u0442\u044c");
            deleteButton.addActionListener(e -> tablePresenter.deleteButtonClicked(table1.getSelectedRow()));
            panel1.add(deleteButton, CC.xy(5, 1));

            //---- beginButton ----
            beginButton.setText("<<-");
            beginButton.addActionListener(e -> tablePresenter.toBeginButtonClicked());
            panel1.add(beginButton, CC.xy(9, 1));

            //---- prevButton ----
            prevButton.setText("<-");
            prevButton.addActionListener(e -> tablePresenter.prevButtonClicked());
            panel1.add(prevButton, CC.xy(11, 1));

            //---- nextButton ----
            nextButton.setText("->");
            nextButton.addActionListener(e -> tablePresenter.nextButtonClicked());
            panel1.add(nextButton, CC.xy(13, 1));

            //---- endButton ----
            endButton.setText("->>");
            endButton.addActionListener(e -> tablePresenter.toLastButtonClicked());
            panel1.add(endButton, CC.xy(15, 1));

            //---- viewButton ----
            viewButton.setText("\u0421\u043c\u0435\u043d\u0438\u0442\u044c \u0432\u0438\u0434");
            viewButton.addActionListener(e -> tablePresenter.changeViewTypeClicked(table1.getSelectedRow()));
            panel1.add(viewButton, CC.xy(17, 1));

            //---- exitButton ----
            exitButton.setText("\u0412\u044b\u0445\u043e\u0434");
            exitButton.addActionListener(e -> tablePresenter.onExitClicked());
            panel1.add(exitButton, CC.xy(19, 1));
        }
        contentPane.add(panel1, CC.xy(2, 4));
        setSize(1020, 470);
        setLocationRelativeTo(getOwner());

        //======== panel2 ========
        {
            panel2.setBackground(new Color(217, 217, 217, 232));
            panel2.setLayout(new FormLayout(
                "default, $lcgap, default",
                "2*(default, $lgap), default"));
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel jTitle;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JPanel panel1;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;
    private JButton beginButton;
    private JButton prevButton;
    private JButton nextButton;
    private JButton endButton;
    private JButton viewButton;
    private JButton exitButton;
    private JPanel panel2;

    public void onCancelButtonClicked() {
        tablePresenter.onCancelButtonClicked();
    }

    public void onOkClicked(T object, boolean isAdd) {
        tablePresenter.onOkButtonClicked(object, isAdd);
    }

    @Override
    public void showEditPanel(T objToEdit) {
        addButton.setVisible(false);
        editButton.setVisible(false);
        deleteButton.setVisible(false);

        editPanel.showSaveButton(true);
        editPanel.showCancelButton(true);

        editPanel.setObjectToEdit(objToEdit);
        editPanel.setEnabledElements(true);
        scrollPane1.setViewportView(editPanel);
    }

    @Override
    public void showTable() {
        scrollPane1.setViewportView(table1);

        addButton.setVisible(true);
        editButton.setVisible(true);
        deleteButton.setVisible(true);

//        beginButton.setVisible(false);
//        prevButton.setVisible(false);
//        nextButton.setVisible(false);
//        endButton.setVisible(false);
    }

    @Override
    public void showFoxProView(T curObject) {
        editPanel.showSaveButton(false);
        editPanel.showCancelButton(false);

        addButton.setVisible(true);
        editButton.setVisible(true);
        deleteButton.setVisible(true);

//        beginButton.setVisible(true);
//        prevButton.setVisible(true);
//        nextButton.setVisible(true);
//        endButton.setVisible(true);

        editPanel.setObjectToEdit(curObject);
        editPanel.setEnabledElements(false);
        scrollPane1.setViewportView(editPanel);
    }

    @Override
    public boolean resultErrorDialog() {
        if (table1.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Ничего не выбрано");
            return true;
        }

        return false;
    }

    @Override
    public boolean resultConfirmDialog() {
        return false;
    }

    @Override
    public void update() {
        tableModel.fireTableDataChanged();
    }

    @Override
    public void showData(List<T> data) {
        tableModel.setData(data);
    }

    @Override
    public void setSelectedPosition(int position) {
        table1.setRowSelectionInterval(position, position);
    }

    @Override
    public void exit() {
        setVisible(false);
        dispose();
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
